$(document).ready(function(){ 
	$(".scroll").click(function(event){
		event.preventDefault();
		$("html,body").animate({scrollTop:$(this.hash).offset().top}, 500);
		$('.navbar-default a').removeClass('selected');
		$(this).addClass('selected');
    	});
});

$(document).ready(function(){
    $('.menu-container').hover(
        function(){
            $('.profile-actions').slideDown('fast');
          $('.list-icon').addClass('active');
        },
        function(){
            $('.profile-actions').slideUp('fast');
          $('.list-icon').removeClass('active');
        }
    );
    $('.profile-card').mouseleave(function(){
        $('.profile-actions').slideUp('fast');
        $('.profile-info').slideUp('fast');
        $('.profile-map').slideUp('fast');
    });

    $('.profile-avatar').hover(
        function(){
            $('.profile-links').fadeIn('fast');
        },
        function(){
            $('.profile-links').hide();
        }
    );
    $('.read-more').click(function(){
        $('.profile-map').slideUp('fast');
        $('.profile-info').slideToggle('fast');
        return false;
    });
    $('.view-map').click(function(){
        $('.profile-info').slideUp('fast');
        $('.profile-map').slideToggle('fast');
        return false;
    });
});

$(document).ready(function(){
    
     $("body").on("click","#btn",function(){
            
          $("#myModal").modal("show");
      
          //appending modal background inside the blue div
          $('.modal-backdrop').appendTo('.blue');   
    
          //remove the padding right and modal-open class from the body tag which bootstrap adds when a modal is shown
          $('body').removeClass("modal-open")
          $('body').css("padding-right","");     
      });
 
});